package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;


import com.runner.BaseClass;

public class StoreLocatorPage extends BaseClass {
	
	//Store Locator Functionality
			private static By STORELOCATOR=By.linkText("Tu Store Locator");
			private static By STORELOCATORTEXTBOX=By.cssSelector("input[class='ln-c-text-input ln-u-push-bottom']");
			private static By WOMENCHECKBOX=By.cssSelector("label[for='women']");
			private static By MENCHECKBOX=By.cssSelector("label[for='men']");
			private static By CHILDCHECKBOX=By.cssSelector("label[for='children']");
			private static By CLICKCOLLECTCHECKBOX=By.cssSelector("label[for='click']");
			private static By FINDSTOREBUTTON=By.cssSelector("#tuStoreFinderForm .ln-c-button--primary");
			private static By VIEWSTOREBUTTON=By.cssSelector("a[class='strong']");
			private static By ASSERTH1NAME=By.cssSelector("h1");
			private static By ASSERTNEARESTSTORE=By.cssSelector("#header1");
			private static By ASSERTFORINVALIDPC=By.cssSelector("#globalMessages");
	
	public void userOnStoreLocatorPage() throws InterruptedException {
		action.clickOnTheElement(STORELOCATOR);
		Assert.assertEquals("Store Locator", get.getElementText(ASSERTH1NAME));
		Thread.sleep(3000);
	}
	public void userEntersValidPostcode() throws InterruptedException {
		action.updateElement(STORELOCATORTEXTBOX, "E6 3DP");
        Thread.sleep(3000);
	}
	public void userClicksOnFindStores() {
		action.clickOnTheElement(FINDSTOREBUTTON);
	}
	public void userGetsDetailsOfNearestStores() {
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTNEARESTSTORE));
	}
	public void userEntersInvalidPostCode() throws InterruptedException {
		action.updateElement(STORELOCATORTEXTBOX, "E6 PPP");
        Thread.sleep(3000);
	}
	public void errorMessageDueToInvalidPostcode() {
		System.out.println("Sorry no results found,Please check your Town or Postcode");
		Assert.assertEquals("Sorry, no results found for your search.", get.getElementText(ASSERTFORINVALIDPC));
	}
	public void userEntersTownName() throws InterruptedException {
		action.updateElement(STORELOCATORTEXTBOX, "London");
        Thread.sleep(3000);
        action.clickOnTheElement(WOMENCHECKBOX);
        Thread.sleep(3000);
        action.clickOnTheElement(MENCHECKBOX);
        action.clickOnTheElement(CHILDCHECKBOX);
        action.clickOnTheElement(CLICKCOLLECTCHECKBOX);
        Thread.sleep(3000);
	}
	public void userGetsDetailsOfnearestStoreWithOpeningTime() {
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTNEARESTSTORE));
	}
	public void userLeavesFieldAsBlank() {
		action.updateElement(STORELOCATORTEXTBOX, "");
	}
	public void userGetsErrorDueToBlankdata() {
		System.out.println("Please Fill The Fields");
	}
	public void userSelectForSCAndCC() throws InterruptedException {
		action.updateElement(STORELOCATORTEXTBOX, "E6 3DP");
        Thread.sleep(3000);
        action.clickOnTheElement(WOMENCHECKBOX);
        Thread.sleep(3000);
        action.clickOnTheElement(MENCHECKBOX);
        action.clickOnTheElement(CHILDCHECKBOX);
        action.clickOnTheElement(CLICKCOLLECTCHECKBOX);
        Thread.sleep(3000);
	}
	public void storesWithSCAndCC() {
		Assert.assertEquals("Your nearest stores", get.getElementText(ASSERTNEARESTSTORE));
	}
	public void userEntersWithPCOrTownName() throws InterruptedException {
		action.updateElement(STORELOCATORTEXTBOX, "E6 3DP");
        Thread.sleep(3000);
	}
	public void userSelectVeryNearByStore() {
		action.clickOnTheElementWithIndex(VIEWSTOREBUTTON, 0);
	}
	public void particularStoreDetails() {
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/store/East+Ham?lat=51.5298678&long=0.0419205&q=E6%203DP", get.getcurrentURL());
	}

}
