package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;


import com.runner.BaseClass;

public class BasketPage extends BaseClass {
	private static By SELECTFIRST=By.cssSelector(".image-component img");
	private static By SELECTFIRSTIMAGE=By.cssSelector(".image-component img");
	private static By SIZEDROPDOWN=By.cssSelector("#select-size");
	private static By QUANTITYDROPDOWN=By.cssSelector("#productVariantQty");
	private static By ADDTOBASKETBUTTON=By.cssSelector("#AddToCart");
	private static By VIEWBASKETBUTTON=By.cssSelector("#basket-title");
	private static By VIEWBASKETANDCHECKOUTBUTTON=By.cssSelector("a[class='ln-c-button ln-c-button--primary']");
	//Basket Functionality
			private static By BRANDSLINK=By.linkText("Brands");
			private static By JEWWATCHLINK=By.linkText("JEWELLERY & WATCHES");
			private static By CANDCLINK=By.linkText("Click & Collect");
			private static By AMENDQUANTITYDROPDOWN=By.cssSelector("#quantity0");
			private static By AMENDUPDATEBUTTON=By.linkText("Update");
			private static By FREERETURNSTOSTORELINK=By.cssSelector("a[href='/help/returnsAndRefunds']");
			private static By HOMEDELIVERYLINK=By.linkText("Standard Delivery");
			private static By CONTINUESHOPPINGLINK=By.cssSelector("a[class='ln-c-button ln-c-button--secondary left']");
			private static By ASSERTH1NAME=By.cssSelector("h1");
			private static By ASSERTPRODUCTDETAILS=By.cssSelector("#tu-product-details");
			private static By ASSERTADDPRODUCTBASKET=By.cssSelector(".prod_quantity");
			private static By ASSERTITEMDESCRIPTION=By.cssSelector(".your_cart th");
			private static By DELETEBUTTON=By.cssSelector("a[href='/removeItemFromMiniBasket?entryNumber=1']");
			private static By PROCEEDTOCHECKOUTBUTTON=By.cssSelector("#basketButtonBottom");
			

			public void userOnBasketPToAddPc() throws InterruptedException {
				action.clickOnTheElement(BRANDSLINK);				
				Thread.sleep(3000);
		        action.clickOnTheElement(JEWWATCHLINK);
		        Assert.assertEquals("Jewellery and Watches Brands", get.getElementText(ASSERTH1NAME));
		        Thread.sleep(3000);
		        action.clickOnTheElementWithIndex(SELECTFIRSTIMAGE, 0);
		        Thread.sleep(3000);
		        Assert.assertEquals("Product Details", get.getElementText(ASSERTPRODUCTDETAILS));
		        action.dropDownByIndex(SIZEDROPDOWN, 1);		      
		        Thread.sleep(3000);
		        action.dropDownByIndex(QUANTITYDROPDOWN, 1);
		        Thread.sleep(5000);
		        action.clickOnTheElement(ADDTOBASKETBUTTON);
				Thread.sleep(5000);
				action.clickOnTheElement(VIEWBASKETBUTTON);
		        Thread.sleep(5000);
		        Assert.assertEquals("Qty:2", get.getElementText(ASSERTADDPRODUCTBASKET));
		        action.clickOnTheElement(VIEWBASKETANDCHECKOUTBUTTON);		        
		        Thread.sleep(5000);
		        Assert.assertTrue(get.getElementText(ASSERTITEMDESCRIPTION).contains("Item description"));
		        Thread.sleep(5000);
			}
			public void userClicksOnCAndC() {
				action.clickOnTheElement(CANDCLINK);
			}
			public void detailsOfDeliveryInfo() {
				Assert.assertEquals("Delivery information", get.getElementText(ASSERTH1NAME));
			}
			public void userClicksOnFreeReturnsToStore() {
				action.clickOnTheElement(FREERETURNSTOSTORELINK);
			}
			public void detailsOfReturnsAndRefunds(){
				Assert.assertEquals("Returns & refunds", get.getElementText(ASSERTH1NAME));
			}
			public void userClicksOnHomeDelivery() {
				action.clickOnTheElement(HOMEDELIVERYLINK);
			}
			public void userAmendsTheQuantity() throws InterruptedException {
				action.dropDownByIndex(AMENDQUANTITYDROPDOWN, 3);
		        Thread.sleep(5000);
			}
			public void userClicksOnUpdate() {
				action.clickOnTheElement(AMENDUPDATEBUTTON);
			}
			public void updatedAmendedQuantity() {
				Assert.assertTrue(get.getElementText(ASSERTITEMDESCRIPTION).contains("Item description"));
			}
			public void userRemovingProduct() {
				action.clickOnTheElement(DELETEBUTTON);
			}
			public void userClicksOnContinueShopping() {
				action.clickOnTheElement(CONTINUESHOPPINGLINK);
			}
			public void userDirectsForShopping() {
				Assert.assertEquals("Jewellery and Watches Brands", get.getElementText(ASSERTH1NAME));
			}
			public void userSelectAddToBasket() throws InterruptedException {
				action.clickOnTheElement(ADDTOBASKETBUTTON);
		        Thread.sleep(5000);
			}
			public void userClicksOnViewBasket() throws InterruptedException {
				action.clickOnTheElement(VIEWBASKETBUTTON);
		        Thread.sleep(5000);
		        Assert.assertEquals("Qty:2", get.getElementText(ASSERTADDPRODUCTBASKET));
			}
			public void userProceedToCheckOutButton() throws InterruptedException {
				action.clickOnTheElement(VIEWBASKETANDCHECKOUTBUTTON);
		        Thread.sleep(5000);
		        Assert.assertTrue(get.getElementText(ASSERTITEMDESCRIPTION).contains("Item description"));
		        action.clickOnTheElement(PROCEEDTOCHECKOUTBUTTON);
		        Thread.sleep(5000);	
			}
			
			
			
			
			
}
