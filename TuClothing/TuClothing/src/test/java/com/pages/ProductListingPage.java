package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class ProductListingPage extends BaseClass {
	
	private static By SEARCHTEXTBOX=By.cssSelector("#search");
	private static By SEARCHBUTTON=By.cssSelector(".searchButton");
	private static By ASSERTH1NAME=By.cssSelector("h1");
	private static By ASSERTPRODUCTDETAILS=By.cssSelector("#tu-product-details");
	private static By SELECTFIRSTIMAGE=By.cssSelector(".image-component img");
	
	public void userSelectAProduct() throws InterruptedException {
		action.updateElement(SEARCHTEXTBOX, "Shoes");
        action.clickOnTheElement(SEARCHBUTTON);        
        Thread.sleep(6000);
        Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("shoes"));
        Thread.sleep(3000);
        action.clickOnTheElementWithIndex(SELECTFIRSTIMAGE, 1);
        Thread.sleep(5000);
        Assert.assertEquals("Product Details", get.getElementText(ASSERTPRODUCTDETAILS));
	}

}
