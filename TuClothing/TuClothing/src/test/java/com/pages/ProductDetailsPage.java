package com.pages;



import org.junit.Assert;
import org.openqa.selenium.By;


import com.runner.BaseClass;


public class ProductDetailsPage extends BaseClass {
	
	private static By SEARCHTEXTBOX=By.cssSelector("#search");
	private static By SEARCHBUTTON=By.cssSelector(".searchButton");
	
	//Add to Basket functionality
//	private static By SUMMERLINK=By.linkText("Summer");
//	private static By SWIMWEARIMAGE=By.cssSelector("img[alt='Womens Swimwear']");
//	private static By ANIMALPRINTDRESSIMAGE=By.cssSelector("img[alt='Animal Print Swim Dress ']");
	private static By SIZECHECKBOX=By.cssSelector(".selectable ");
	private static By QUANTITYDROPDOWN=By.cssSelector("#productVariantQty");
	private static By ADDTOBASKETBUTTON=By.cssSelector("#AddToCart");
	private static By VIEWBASKETBUTTON=By.cssSelector("#basket-title");
//	private static By VIEWBASKETANDCHECKOUTBUTTON=By.cssSelector("a[class='ln-c-button ln-c-button--primary']");
//	private static By AMENDQUANTITYDROPDOWN=By.cssSelector("#quantity0");
//	private static By AMENDUPDATEBUTTON=By.linkText("Update");
	private static By NAVYGINGHAMSWIMSUITIMAGE=By.cssSelector("img[alt='Navy Gingham Skirted Swimsuit ']");
	private static By DELETEBUTTON=By.cssSelector("a[href='/removeItemFromMiniBasket?entryNumber=1']");
	private static By LOUNGEWEARLINK=By.linkText("Loungewear");
	private static By SELECTFIRSTIMAGE=By.cssSelector(".image-component img");
	private static By SIZEDROPDOWN=By.cssSelector("#select-size");
	private static By BABYLINK=By.linkText("Baby");
	private static By NEWARRIVALSLINK=By.linkText("New Arrivals");
	private static By ASSERTLOUNGEWEAR=By.cssSelector("#js-plp-category h1");
	private static By ASSERTPRODUCTDETAILS=By.cssSelector("#tu-product-details");
	private static By ASSERTADDPRODUCTBASKET=By.cssSelector(".prod_quantity");
	private static By ASSERTH1NAME=By.cssSelector("h1");
	private static By ASSERTAEROPLANE=By.cssSelector(".ln-u-flush-bottom span");

	public void userOnProductDetailsPage() throws InterruptedException {
		action.clickOnTheElement(LOUNGEWEARLINK);
		Assert.assertEquals("Loungewear", get.getElementText(ASSERTLOUNGEWEAR));
		Thread.sleep(3000);
		action.clickOnTheElementWithIndex(SELECTFIRSTIMAGE, 2);
		Thread.sleep(3000);
		Assert.assertEquals("Product Details", get.getElementText(ASSERTPRODUCTDETAILS));
	}

	public void userSelectSizeAndQuantity() throws InterruptedException {
		action.dropDownByIndex(SIZEDROPDOWN, 1);
		Thread.sleep(3000);
		action.dropDownByIndex(QUANTITYDROPDOWN, 2);
	}

	public void userClicksOnAddToBasket() throws InterruptedException {
		action.clickOnTheElement(ADDTOBASKETBUTTON);
		Thread.sleep(3000);
		action.clickOnTheElement(VIEWBASKETBUTTON);
	}

	public void productShouldAddToBasket() throws InterruptedException {
		Thread.sleep(3000);
		Assert.assertEquals("Qty:3", get.getElementText(ASSERTADDPRODUCTBASKET));
	}

	public void userSelectSizeAndMultipleQuantities() throws InterruptedException {
		action.dropDownByIndex(SIZEDROPDOWN, 1);
		Thread.sleep(3000);
		action.dropDownByIndex(QUANTITYDROPDOWN, 2);
		action.clickOnTheElement(ADDTOBASKETBUTTON);
		Thread.sleep(3000);
		action.clickOnTheElement(BABYLINK);
		Thread.sleep(5000);
		action.clickOnTheElement(NEWARRIVALSLINK);
		Assert.assertEquals("Baby Clothing New In", get.getElementText(ASSERTH1NAME));
		Thread.sleep(5000);
		action.clickOnTheElementWithIndex(SELECTFIRSTIMAGE, 0);
		Thread.sleep(5000);
		Assert.assertEquals("Areoplane Print 1.5 Tog Sleeping Bag", get.getElementText(ASSERTAEROPLANE));
		action.dropDownByIndex(SIZEDROPDOWN, 1);
		Thread.sleep(5000);
		action.dropDownByIndex(QUANTITYDROPDOWN, 1);
	}

	public void productAddedToBasketWithMultipleQuantity() {
		Assert.assertEquals("Qty:2", get.getElementText(ASSERTADDPRODUCTBASKET));
	}

	public void userSelectRemoveProductOption() throws InterruptedException {
		Thread.sleep(3000);
		action.clickOnTheElementWithIndex(SIZECHECKBOX, 1);
		Thread.sleep(3000);
		action.dropDownByIndex(QUANTITYDROPDOWN, 1);
		Thread.sleep(3000);
		action.clickOnTheElement(ADDTOBASKETBUTTON);
		Thread.sleep(3000);
		action.updateElement(SEARCHTEXTBOX, "Navy Gingham Skirted Swimsuit ");
		action.clickOnTheElement(SEARCHBUTTON);
		Thread.sleep(3000);
		action.clickOnTheElement(NAVYGINGHAMSWIMSUITIMAGE);	
		Assert.assertEquals("Navy Gingham Skirted Swimsuit", get.getElementText(ASSERTAEROPLANE));
		Thread.sleep(3000);
		action.clickOnTheElementWithIndex(SIZECHECKBOX, 2);
		Thread.sleep(3000);
		action.dropDownByIndex(QUANTITYDROPDOWN, 1);
		action.clickOnTheElement(ADDTOBASKETBUTTON);
		Thread.sleep(5000);
		action.clickOnTheElement(VIEWBASKETBUTTON);
		Thread.sleep(4000);
		action.clickOnTheElement(DELETEBUTTON);
	}
	public void userSelectProductSizeAndQuantity() throws InterruptedException {
		action.clickOnTheElementWithIndex(SIZECHECKBOX, 3);
        Thread.sleep(3000);
        action.dropDownByIndex(QUANTITYDROPDOWN, 1);
        Thread.sleep(5000);
	}


}
