package com.pages;

import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;


import com.runner.BaseClass;

import cucumber.api.DataTable;

public class LoginRegisterPage extends BaseClass {
	
	private static String baseURL= "https://tuclothing.sainsburys.co.uk/";
	//Login Functionality
	private static By LOGINREGISTER=By.linkText("Tu Log In / Register");
	private static By EMAILID=By.cssSelector("#j_username");
	private static By PASSWORD=By.cssSelector("#j_password");
	private static By HIDESHOW=By.cssSelector(".hide-show-button");
	private static By FPASSWORD=By.cssSelector("#forgot-password-link");
	private static By SUBMITLOGIN=By.cssSelector("#submit-login");
	private static By ASSERTLOGINRETURNING=By.cssSelector(".loginFormHolder h2");
	private static By ASSERTREGISTERWITHTU=By.cssSelector(".span-12.last h2");
//	private static By ASSERTWITHRECAPTCHA=By.cssSelector("#reCaptcha-register");
	//Register Functionality
	private static By REGISTERBUTTON=By.cssSelector(".regToggle");
	private static By REMAILADDRESS=By.cssSelector("#register_email");
	private static By RTITLE=By.cssSelector("#register_title");
	private static By RFIRSTNAME=By.cssSelector("#register_firstName");
	private static By RLASTNAME=By.cssSelector("#register_lastName");
	private static By RPASSWORD=By.cssSelector("#password");
	private static By RCONFIRMPASSWORD=By.cssSelector("#register_checkPwd");
	private static By RNECTARNUMBER=By.cssSelector("#regNectarPointsOne");
	private static By RCOMPLETEREGISTER=By.cssSelector("#submit-register");
	
			
	public void userEnterValidCredentials(String email, String password) {//Used DataTable Maps
		action.updateElement(EMAILID, email);
		action.updateElement(PASSWORD, password);
	}
	public void userEnterInvalidCredentials() throws InterruptedException {
		Thread.sleep(4000);
		action.updateElement(EMAILID, "test90#@dne.co");
        Thread.sleep(3000);
        action.updateElement(PASSWORD, "S123ghjkh*8");
	}
   public void errormessageduetoInvalidCredentials() {
	   Assert.assertEquals("Returning Tu customers", get.getElementText(ASSERTLOGINRETURNING));
	  }
   public void userenterValidIdandInvalidPassword() throws InterruptedException {
	   Thread.sleep(3000);
	   action.updateElement(EMAILID, "sherinsunny17@gmail.com");
	   action.updateElement(PASSWORD, "*****ghj");
   }
   public void errormessageduetoInvalidPassword() {
	   Assert.assertEquals("Returning Tu customers", get.getElementText(ASSERTLOGINRETURNING));
		System.out.println("Warning Message Displayed");
   }
   public void userenterInvalidIdandValidPassword() throws InterruptedException {
	   Thread.sleep(2000);
	   action.updateElement(EMAILID, "test90#@dne.co");
       action.updateElement(PASSWORD, "apple123");
   }
   public void errormessageduetoInvalidId() {
	   Assert.assertEquals("Returning Tu customers", get.getElementText(ASSERTLOGINRETURNING));
		System.out.println("Warning Message Displayed");
   }
   public void userClickOnForgottenYourPassword() throws InterruptedException {
	   Thread.sleep(3000);
	   action.updateElement(EMAILID, "sherinsunny17@gmail.com");
	   action.updateElement(PASSWORD, "");
       action.clickOnTheElement(FPASSWORD);
   }
   public void userDirectToForgottenYourPasswordPage() {
	   Assert.assertEquals("Returning Tu customers", get.getElementText(ASSERTLOGINRETURNING));
		System.out.println("Warning Message Displayed");
   }
   public void userkeptIdAndPasswordAsBlank() throws InterruptedException {
	   Thread.sleep(3000);
	   action.updateElement(EMAILID,"");
	   action.updateElement(PASSWORD, ""); 
   }
   public void userGetErrorMessageDueToBlankdata() {
	   Assert.assertEquals("Returning Tu customers", get.getElementText(ASSERTLOGINRETURNING));
		System.out.println("Warning Message Displayed");
   }
   public void userEnterValidIdAndValidPassword() throws InterruptedException {
	   Thread.sleep(3000);
	   action.updateElement(EMAILID, "sherinsunny17@gmail.com");
	   action.updateElement(PASSWORD, "apple123");
       action.clickOnTheElement(SUBMITLOGIN);
   }
   public void errorMessageDueToWIthoutRecaptcha() {
	   Assert.assertEquals("Returning Tu customers", get.getElementText(ASSERTLOGINRETURNING));
		System.out.println("Please Check The Recaptcha");
   }
   public void userClickOnShowPassword() throws InterruptedException {
	   Thread.sleep(3000);
	   action.updateElement(EMAILID, "sherinsunny17@gmail.com");
	   action.updateElement(PASSWORD, "apple123");
       Thread.sleep(3000);
       action.clickOnTheElement(HIDESHOW);
   }
   public void userSeeThePasswordAsBulletPoints() {
	   Assert.assertEquals("Returning Tu customers", get.getElementText(ASSERTLOGINRETURNING)); 
   }
   //REGISTER FUNCTIONALITY
   public void useronRegistrationPage() throws InterruptedException {
	   System.setProperty("webdriver.chrome.driver", "E:\\Workspaces\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
       driver.get(baseURL);
		driver.manage().window().maximize();
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getcurrentURL());  
		Thread.sleep(3000);
		action.clickOnTheElement(LOGINREGISTER);
		Thread.sleep(4000);
		action.clickOnTheElement(REGISTERBUTTON);
   }
   public void userclicksonCRegisterbutton() throws InterruptedException {
	   Thread.sleep(2000);
	   action.clickOnTheElement(RCOMPLETEREGISTER);
	  }
   public void errorMessageForBlankDataonReg() {
	   Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
		System.out.println("Please Fill The Field");
   }
   public void userEntersFieldWithInvalidemail() throws InterruptedException {
	   action.updateElement(REMAILADDRESS, "Test@.com");
	   action.dropDownByIndex(RTITLE, 3);
	   Thread.sleep(2000);
	   action.updateElement(RFIRSTNAME, "Test12&@?");
	   action.updateElement(RLASTNAME, "Test12&");
	   action.updateElement(RPASSWORD, "tatabirla1234");
	   action.updateElement(RCONFIRMPASSWORD, "tatabirla123");
	   action.updateElement(RNECTARNUMBER, "45678964");
   }
   public void warningmessagedueToInvalidEmail() {
	   Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
		System.out.println("Please Check The Fields");
   }
   public void userEntersWithSpecialCharacters() throws InterruptedException {
	   action.updateElement(REMAILADDRESS, "Test@test.com");
	   action.dropDownByIndex(RTITLE, 3);
       Thread.sleep(2000);
       action.updateElement(RFIRSTNAME, "Test12&@?");
	   action.updateElement(RLASTNAME, "Test12&");
	   action.updateElement(RPASSWORD, "tatabirla1234");
	   action.updateElement(RCONFIRMPASSWORD, "tatabirla123");
	   action.updateElement(RNECTARNUMBER, "45678964");
   }
   public void warningMessageDueToSpecialCharacters() {
	   Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
		System.out.println("Please Check The Fields");
   }
   public void userEntersWithPasswordLessThan6Characters() throws InterruptedException {
	   action.updateElement(REMAILADDRESS, "Test@test.com");
	   action.dropDownByIndex(RTITLE, 3);
       Thread.sleep(2000);
       action.updateElement(RFIRSTNAME, "Test12&@?");
	   action.updateElement(RLASTNAME, "Test12&");
	   action.updateElement(RPASSWORD, "tat");
	   action.updateElement(RCONFIRMPASSWORD, "tat");
	   action.updateElement(RNECTARNUMBER, "45678964");
   }
   public void warningMessageForPasswordLessThan6Characters() {
	   Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU));
		System.out.println("Please Check The Fields"); 
   }
   public void userEntersWithNectarCard() throws InterruptedException {
	   action.updateElement(REMAILADDRESS, "Test@test.com");
	   action.dropDownByIndex(RTITLE, 3);
       Thread.sleep(2000);
       action.updateElement(RFIRSTNAME, "Tata");
	   action.updateElement(RLASTNAME, "Birla");
	   action.updateElement(RPASSWORD, "tat");
	   action.updateElement(RCONFIRMPASSWORD, "tat");
	   action.updateElement(RNECTARNUMBER, "45678964");
   }
   public void userDirectToHomePage() {
	   Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU)); //wont direct to home page because of recaptcha
   }
   public void userEntersWithValidCredentialsForRegister(DataTable userData) throws InterruptedException {
	   Map<String,String> registerDetails = userData.asMap(String.class, String.class);
	   String EmailValue = registerDetails.get("email");
	   String FirstName = registerDetails.get("firstName");
	   String LastName = registerDetails.get("lastName");
	   String Password = registerDetails.get("password");
	   String ConfirmPassword = registerDetails.get("confirmPassword");
	   String NectarNum = registerDetails.get("nectarNumber");
	   action.updateElement(REMAILADDRESS, EmailValue);
	   action.dropDownByIndex(RTITLE, 3);
       Thread.sleep(2000);
       action.updateElement(RFIRSTNAME, FirstName);
	   action.updateElement(RLASTNAME, LastName);
	   action.updateElement(RPASSWORD, Password);
	   action.updateElement(RCONFIRMPASSWORD, ConfirmPassword);
	   action.updateElement(RNECTARNUMBER, NectarNum);
	   
   }
   public void userGetSuccessfulNotification() {
	   Assert.assertEquals("Register with Tu", get.getElementText(ASSERTREGISTERWITHTU)); //wont direct to home page because of recaptcha
   }
   public void userEntersDataWithoutRecaptcha() throws InterruptedException {
	   action.updateElement(REMAILADDRESS, "Test@test.com");
	   action.dropDownByIndex(RTITLE, 3);
       Thread.sleep(2000);
       action.updateElement(RFIRSTNAME, "Tata");
	   action.updateElement(RLASTNAME, "Birla");
	   action.updateElement(RPASSWORD, "tatabirla1234");
	   action.updateElement(RCONFIRMPASSWORD, "tatabirla1234");
	   action.updateElement(RNECTARNUMBER, "45678964");
   }
   public void errorMessageWithoutRecaptcha() {
	   Assert.assertEquals("Please check the recaptcha", get.getElementText(ASSERTREGISTERWITHTU));
		System.out.println("Please Check The Recaptcha");
   }
}
