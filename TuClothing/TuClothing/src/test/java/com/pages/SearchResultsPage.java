package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;



public class SearchResultsPage extends BaseClass {
	private static By ASSERTH1NAME=By.cssSelector("h1");
	private static By ASSERTWELCOMEMESSAGE=By.cssSelector(".loginFormHolder h2");
	
	public void searchResultsforValidProduct() {
		Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("tops"));  
	}
	public void searchResultsforInvalidProduct() {
		Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("liquor"));
        System.out.println("Sorry No Results Found");
	}
	public void searchResultsforProductCode() {
		Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("137430191"));
	}
    public void searchResultsforProductColor() {
    	Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("black"));
    }
    public void searchResultsforProductDescription() {
    	Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("yellow oversized knitted top"));
    }
    public void searchResultsforSpecialCharacters() {
    	Assert.assertTrue(get.getElementText(ASSERTH1NAME).contains("@\\!&"));
        System.out.println("Sorry No results found");//Random products displayed instead of showing error message
    }
    public void welcomeMessageWithName() { //IT WONT WORK HERE BECAUSE OF RECAPTCHA
    	Assert.assertEquals("Returning Tu customers", get.getElementText(ASSERTWELCOMEMESSAGE));
    }
}
