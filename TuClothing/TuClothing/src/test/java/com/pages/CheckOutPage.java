package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;


import com.runner.BaseClass;

public class CheckOutPage extends BaseClass {
	
	private static By SELECTFIRSTIMAGE=By.cssSelector(".image-component img");
//	private static By SIZEDROPDOWN=By.cssSelector("#select-size");
	private static By SIZECHECKBOX=By.cssSelector(".selectable ");
	private static By QUANTITYDROPDOWN=By.cssSelector("#productVariantQty");
	private static By ADDTOBASKETBUTTON=By.cssSelector("#AddToCart");
	private static By VIEWBASKETBUTTON=By.cssSelector("#basket-title");
	private static By VIEWBASKETANDCHECKOUTBUTTON=By.cssSelector("a[class='ln-c-button ln-c-button--primary']");
	//Basket Functionality
//			private static By BRANDSLINK=By.linkText("Brands");
//			private static By JEWWATCHLINK=By.linkText("JEWELLERY & WATCHES");
			private static By PROCEEDTOCHECKOUTBUTTON=By.cssSelector("#basketButtonBottom");
//			private static By CANDCLINK=By.linkText("Click & Collect");
//			private static By AMENDQUANTITYDROPDOWN=By.cssSelector("#quantity0");
//			private static By AMENDUPDATEBUTTON=By.linkText("Update");
//			private static By FREERETURNSTOSTORELINK=By.cssSelector("a[href='/help/returnsAndRefunds']");
//			private static By HOMEDELIVERYLINK=By.linkText("Standard Delivery");
//			private static By CONTINUESHOPPINGLINK=By.cssSelector("a[class='ln-c-button ln-c-button--secondary left']");
			private static By ASSERTH1NAME=By.cssSelector("h1");
			private static By ASSERTPRODUCTDETAILS=By.cssSelector("#tu-product-details");
			private static By ASSERTADDPRODUCTBASKET=By.cssSelector(".prod_quantity");
			private static By ASSERTITEMDESCRIPTION=By.cssSelector(".your_cart th");
//			private static By DELETEBUTTON=By.cssSelector("a[href='/removeItemFromMiniBasket?entryNumber=1']");
	//Check Out Functionality
			private static By GUESTEMAILTEXTBOX=By.cssSelector("#guest_email");
			private static By GUESTCHECKOUTBUTTON=By.cssSelector("button[data-testid='guest_checkout']");
			private static By CANDCRADIOBUTTON=By.cssSelector("label[for='CLICK_AND_COLLECT']");
			private static By DELIVERYCONTINUEBUTTON=By.cssSelector("input[data-testid='continue']");
			private static By POSTCODETEXTBOX=By.cssSelector("#lookup");
			private static By LOOKUPBUTTON=By.cssSelector("span[data-testid='button-text']");
			private static By SELECTSTOREBUTTON=By.cssSelector("button[data-testid='select-store']");
			private static By PROCEEDTOSUMMARYBUTTON=By.cssSelector("button[data-testid='submit-button']");
			private static By CONTINUETOPAYMENTBUTTON=By.cssSelector("button[data-testid='continueToPayment']");
			private static By PAYWITHCARDLINK=By.linkText("Pay with a card");
			private static By TITLEDROPDOWN=By.cssSelector(".ln-c-form-group.ln-u-push-bottom .ln-c-select");
			private static By BFIRSTNAME=By.cssSelector("input[name='newFirstName']");
			private static By BLASTNAME=By.cssSelector("input[name='newSurname']");
			private static By BPOSTCODE=By.cssSelector("input[name='addressPostcode']");
			private static By BFINDADDRESS=By.cssSelector("button[class='ln-c-button ln-c-button--primary']");
			private static By ADDRESSDROPDOWN=By.cssSelector("#addressListView");
//			private static By CONTACTYOUCHECKBOX=By.cssSelector("label[for='contactPreferencesId']");
//			private static By TERMSCHECKBOX=By.cssSelector("label[for='termsAndConditionsId']");
//			private static By FINALCONTINUETOPAYMENTBUTTON=By.cssSelector("#contPayment");
//			private static By RETURNINGCUSTEMAIL=By.cssSelector("#j_username");
//			private static By RETURNINGCUSTPASSWORD=By.cssSelector("#j_password");
			private static By HOMEDELIVERYRADIOBUTTON=By.cssSelector("label[for='HOME_DELIVERY']");
			private static By HDTITLEDROPDOWN=By.cssSelector(".ln-c-form-group.ln-u-push-bottom .ln-c-select.ln-u-push-bottom");
			private static By HDFIRSTNAME=By.cssSelector("input[name='firstName']");
			private static By HDLASTNAME=By.cssSelector("input[name='lastName']");
			private static By HDPOSTCODE=By.cssSelector("input[name='postcode']");
			private static By HDADDRESSLOOKUP=By.cssSelector("button[class='ln-c-button ln-c-button--primary address-lookup ln-u-push-bottom']");
			private static By USEDELIVERYADDFORBILLINGADD=By.cssSelector("label[for='useDeliveryAddressForBillingAddress']");
			private static By CONTINUEDELIVERYBUTTON=By.cssSelector("#continue");
			private static By STANDARDDRADIOBUTTON=By.cssSelector("label[for='standard-delivery']");
			private static By CONTINUEFROMDELIVERYPAGEBUTTON=By.cssSelector("input[value='Continue']");
			private static By SEARCHTEXTBOX=By.cssSelector("#search");
			private static By SEARCHBUTTON=By.cssSelector(".searchButton");
			private static By ASSERTDELIVERYOPTIONS=By.cssSelector(".checkout-step-title");
			private static By ASSERTPAYMENT=By.cssSelector(".ln-u-push-bottom h3");
	
	public void userOnCheckOutPage() throws InterruptedException {
		action.updateElement(SEARCHTEXTBOX, "Shoes");
		action.clickOnTheElement(SEARCHBUTTON);        
        Thread.sleep(6000);
        Assert.assertTrue(get.getElementText(ASSERTH1NAME).toLowerCase().contains("shoes"));
        Thread.sleep(3000);
        action.clickOnTheElementWithIndex(SELECTFIRSTIMAGE, 1);
        Thread.sleep(5000);
        Assert.assertEquals("Product Details", get.getElementText(ASSERTPRODUCTDETAILS));
        action.clickOnTheElementWithIndex(SIZECHECKBOX, 3);
        Thread.sleep(3000);
        action.dropDownByIndex(QUANTITYDROPDOWN, 1);
        Thread.sleep(5000);
        action.clickOnTheElement(ADDTOBASKETBUTTON);
        Thread.sleep(5000);
        action.clickOnTheElement(VIEWBASKETBUTTON);
        Thread.sleep(5000);
        Assert.assertEquals("Qty:2", get.getElementText(ASSERTADDPRODUCTBASKET));
        action.clickOnTheElement(VIEWBASKETANDCHECKOUTBUTTON);
        Thread.sleep(5000);
        Assert.assertTrue(get.getElementText(ASSERTITEMDESCRIPTION).contains("Item description"));
        action.clickOnTheElement(PROCEEDTOCHECKOUTBUTTON);
        Thread.sleep(5000);	
	}
	public void userEnterGuestDetails() throws InterruptedException {
		action.updateElement(GUESTEMAILTEXTBOX, "12Tom@test.com");
        Thread.sleep(2000);
	}
	public void userClicksOnContinue() {
		action.clickOnTheElement(GUESTCHECKOUTBUTTON); //MAKE SURE YOU ARE PURCHASING AN ITEM WHICH COST BELOW �20 INORDER TO DISABLE THE CLICK AND COLLECT OPTION<FOR THIS CASE THE ITEM IS QUALIFIED FOR CLICK AND COLLECT
        Assert.assertEquals("Delivery options", get.getElementText(ASSERTDELIVERYOPTIONS));
	}
	public void disabledOption() {
		System.out.println("Click&Collect is disabled as the purchased amount is below �20.00");
	}
	public void clicksOnContinueForHDDelivery() throws InterruptedException {
		 action.clickOnTheElement(HOMEDELIVERYRADIOBUTTON);
	        Thread.sleep(3000);
	}
	public void homeDeliveryOption() throws InterruptedException {
		action.clickOnTheElement(DELIVERYCONTINUEBUTTON);
        Thread.sleep(3000);
	}
	public void okForHD() {
		System.out.println("USER CAN CONTINUE TO COMPLETE CHECKOUT");
	}
	public void clicksOnCCDelivery() throws InterruptedException {
		action.clickOnTheElement(CANDCRADIOBUTTON);
        Thread.sleep(3000);
	}
	public void clickandCollectOption() throws InterruptedException {
		action.clickOnTheElement(DELIVERYCONTINUEBUTTON);
        Thread.sleep(3000);
	}
	public void okForCC() {
		System.out.println("USER CAN CONTINUE TO COMPLETE CHECKOUT");
	}
	public void selectCCAndContinue() throws InterruptedException {
		action.clickOnTheElement(CANDCRADIOBUTTON);
		action.clickOnTheElement(DELIVERYCONTINUEBUTTON);
        Thread.sleep(3000);
	}
	public void userEnterPostCode() throws InterruptedException {
		action.clickOnTheElement(POSTCODETEXTBOX);
		action.updateElement(POSTCODETEXTBOX, "E6 3DP");
        action.clickOnTheElement(LOOKUPBUTTON);
        Thread.sleep(3000);
        action.clickOnTheElementWithIndex(SELECTSTOREBUTTON, 0);
        Thread.sleep(2000);
	}
	public void clickOnProceedToPayment() throws InterruptedException {
		action.clickOnTheElement(PROCEEDTOSUMMARYBUTTON);
        Thread.sleep(2000);
        action.clickOnTheElement(CONTINUETOPAYMENTBUTTON);
        Thread.sleep(2000);
	}
	public void enterCardDetails() throws InterruptedException {
		action.clickOnTheElement(PAYWITHCARDLINK);
        action.dropDownByIndex(TITLEDROPDOWN, 6);
        Thread.sleep(3000);
        action.updateElement(BFIRSTNAME, "swathy");
        Thread.sleep(3000);
        action.updateElement(BLASTNAME, "kuruvi");
        Thread.sleep(3000);
        action.updateElement(BPOSTCODE, "E6 3DP");
        Thread.sleep(3000);
        action.clickOnTheElement(BFINDADDRESS);
        Thread.sleep(5000);
        action.dropDownByIndex(ADDRESSDROPDOWN, 19);
        Thread.sleep(3000);
	}
	public void clicksOnPayment() {
		//driver.findElement(CONTACTYOUCHECKBOX).click();
        Assert.assertTrue(get.getElementText(ASSERTPAYMENT).contains("Staying in touch"));
        //driver.findElement(TERMSCHECKBOX).click();
        //driver.findElement(FINALCONTINUETOPAYMENTBUTTON).click();	(#####CANT CLICK BECAUSE OF TERMS AND CONDITIONS#####)
	}
	public void paymentConfirmation() {
		System.out.println("THANK YOU FOR SHOPPING WITH US");
	}
	public void selectHDAndContinue() throws InterruptedException {
		action.clickOnTheElement(HOMEDELIVERYRADIOBUTTON);
        Thread.sleep(3000);
        action.clickOnTheElement(DELIVERYCONTINUEBUTTON);
        Thread.sleep(3000);
	}
	public void userEnterMandatoryField() throws InterruptedException {
		action.dropDownByIndex(HDTITLEDROPDOWN, 6);
        Thread.sleep(3000);
        action.updateElement(HDFIRSTNAME, "swathy");
        Thread.sleep(3000);
        action.updateElement(HDLASTNAME, "kuruvi");
        Thread.sleep(3000);
        action.updateElement(HDPOSTCODE, "E6 3DP");
        Thread.sleep(3000);
        action.clickOnTheElement(HDADDRESSLOOKUP);
        Thread.sleep(5000);
        action.dropDownByIndex(ADDRESSDROPDOWN, 19);
        Thread.sleep(3000);
        action.clickOnTheElement(USEDELIVERYADDFORBILLINGADD);
        action.clickOnTheElement(CONTINUEDELIVERYBUTTON);
        Thread.sleep(3000); 
	}
	public void selectHD() throws InterruptedException {
		action.clickOnTheElement(STANDARDDRADIOBUTTON);
        action.clickOnTheElement(CONTINUEFROMDELIVERYPAGEBUTTON);
        Thread.sleep(3000);
        action.clickOnTheElement(CONTINUETOPAYMENTBUTTON);
        Thread.sleep(3000);
        action.clickOnTheElement(PAYWITHCARDLINK);
        action.clickOnTheElement(BFINDADDRESS);
        Thread.sleep(3000);
	}
	
	

}
