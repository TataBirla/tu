package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

import com.runner.BaseClass;

public class HomePage extends BaseClass {
	
	private static String baseURL= "https://tuclothing.sainsburys.co.uk/";
	//Search Functionality
	private static By SEARCHTEXTBOX=By.cssSelector("#search");
	private static By SEARCHBUTTON=By.cssSelector(".searchButton");
	private static By ERRORMESSAGEFORBLANKDATA=By.cssSelector("#search-empty-errors");
	//Login Functionality
	private static By LOGINREGISTER=By.linkText("Tu Log In / Register");
	
	
	//Search Functionality
	public void verifyHomePage() {
	System.setProperty("webdriver.chrome.driver", "E:\\Workspaces\\chromedriver_win32\\chromedriver.exe");
	driver = new ChromeDriver();
    driver.get(baseURL);
	driver.manage().window().maximize();
	Assert.assertEquals("https://tuclothing.sainsburys.co.uk/", get.getcurrentURL());  
}
	public void searchforValidProduct(String searchWord) {
		action.updateElement(SEARCHTEXTBOX, searchWord);
		action.clickOnTheElement(SEARCHBUTTON); 
	}
    public void searchforinvalidproduct() {
    	action.updateElement(SEARCHTEXTBOX, "liquor");
    	action.clickOnTheElement(SEARCHBUTTON);
    }
    public void searchwithblankdata() {
    	action.updateElement(SEARCHTEXTBOX, "");
    	action.clickOnTheElement(SEARCHBUTTON);
    }
    public void promptMessageForBlankData() {
    	Assert.assertEquals("Please complete a product search", get.getElementText(ERRORMESSAGEFORBLANKDATA));
        System.out.println("Please complete a product search"); 
    }
    public void searchWithProductCode() {
    	action.updateElement(SEARCHTEXTBOX, "137430191");
    	action.clickOnTheElement(SEARCHBUTTON);
    }
    public void searchWithProductColor() {
    	action.updateElement(SEARCHTEXTBOX, "Black");
    	action.clickOnTheElement(SEARCHBUTTON);
    }
     public void searchwithProductDescription() {
    	 action.updateElement(SEARCHTEXTBOX, "Yellow Oversized Knitted Top");
    	 action.clickOnTheElement(SEARCHBUTTON);
     }
     public void searchwithSpecialCharacters() {
    	 action.updateElement(SEARCHTEXTBOX, "@#\\!&");
    	 action.clickOnTheElement(SEARCHBUTTON);
         
     }
     //Login Functionality
     public void userClicksonLoginRegisterButton() {
    	 action.clickOnTheElement(LOGINREGISTER);
     }
     
}
