package com.stepDefinitions;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ProductDetailsPageStepDef extends BaseClass{
	
	//ADD PRODUCT TO BASKET FUNCTIONALITY
		@Given("^User is on product details page$")
		public void user_is_on_product_details_page() throws Throwable {
			homepage.verifyHomePage();
			productDetails.userOnProductDetailsPage();
		}
		@When("^User select size and quantity$")
		public void user_select_size_and_quantity() throws Throwable {
			productDetails.userSelectSizeAndQuantity();
		}
		@When("^User click on add to basket$")
		public void user_click_on_add_to_basket() throws Throwable {
			productDetails.userClicksOnAddToBasket();
		}
		@Then("^Product should add to the basket$")
		public void product_should_add_to_the_basket() throws Throwable {
			productDetails.productShouldAddToBasket();
		}
		@Given("^User is already logged in and is on product details page$")//Login Details didnt add because of recaptcha
	 	public void user_is_already_logged_in_and_is_on_product_details_page() throws Throwable {
	 		homepage.verifyHomePage();
	 		productDetails.userOnProductDetailsPage();
	 	}
		@Then("^Product should add to the basket with login details$")//Login Details didnt add because of recaptcha
		public void product_should_add_to_the_basket_with_login_details() throws Throwable {
			productDetails.productShouldAddToBasket();
		}
		@When("^User select size and multiple quantities$")
		public void user_select_size_and_multiple_quantities() throws Throwable {
			productDetails.userSelectSizeAndMultipleQuantities(); 
		}
		@Then("^Product should add to the basket with multiple quantity$")
		public void product_should_add_to_the_basket_with_multiple_quantity() throws Throwable {
			productDetails.productAddedToBasketWithMultipleQuantity();
		}
		@When("^User select remove product option$")
		public void user_select_remove_product_option() throws Throwable {
			productDetails.userSelectRemoveProductOption();
		}
		@Then("^Product should remove from the basket$")
		public void product_should_remove_from_the_basket() throws Throwable {
		    //NEEDS TO DO THE ASSERTION OF LAST PAGE
		}
		@When("^User selects product size and quantity$")
		public void user_selects_product_size_and_quantity() throws Throwable {
			productDetails.userSelectProductSizeAndQuantity();
		}

	
}
