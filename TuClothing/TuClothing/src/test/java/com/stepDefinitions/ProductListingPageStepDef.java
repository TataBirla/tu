package com.stepDefinitions;

import com.runner.BaseClass;

import cucumber.api.java.en.When;

public class ProductListingPageStepDef extends BaseClass {

	@When("^User select a product$")
	public void user_select_a_product() throws Throwable {
		productListing.userSelectAProduct();
	}
}
