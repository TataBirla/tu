package com.stepDefinitions;




import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CheckOutPageStepDef extends BaseClass {
	//CHECKOUT FUNCTIONALITY
		@Given("^User is on the checkout page$")
		public void user_is_on_the_checkout_page() throws Throwable {
			homepage.verifyHomePage();
			checkOut.userOnCheckOutPage();
		}
		@When("^User enters registered credentials to proceed$")
		public void user_enters_registered_credentials_to_proceed() throws Throwable {
			checkOut.userEnterGuestDetails();
		}
		@When("^User clicks on continue$")
		public void user_clicks_on_continue() throws Throwable {
			checkOut.userClicksOnContinue();
		}
		@Then("^User should get disabled option for click & collect$")
		public void user_should_get_disabled_option_for_click_collect() throws Throwable {
			checkOut.disabledOption();
		}
		@When("^User choose the field for new guest and enters the new email id$")
		public void user_choose_the_field_for_new_guest_and_enters_the_new_email_id() throws Throwable {
			checkOut.userEnterGuestDetails();
			checkOut.userClicksOnContinue();
		}
		@When("^User clicks on continue for delivery$")
		public void user_clicks_on_continue_for_delivery() throws Throwable {
			checkOut.clicksOnContinueForHDDelivery();
		}
		@When("^User selects home delivery option$")
		public void user_selects_home_delivery_option() throws Throwable {
			checkOut.homeDeliveryOption();
		}
		@Then("^User should get an enabled option to continue to check out$")
		public void user_should_get_an_enabled_option_to_continue_to_check_out() throws Throwable {
			checkOut.okForHD();
		}
		@When("^User clicks on continue for click & collect$")
		public void user_clicks_on_continue_for_click_collect() throws Throwable {
			checkOut.clicksOnCCDelivery();
		}
		@When("^User selects click & collect option$")
		public void user_selects_click_collect_option() throws Throwable {
			checkOut.clickandCollectOption();
		}
		@Then("^User should get an enabled option to continue to check out to proceed$")
		public void user_should_get_an_enabled_option_to_continue_to_check_out_to_proceed() throws Throwable {
			checkOut.okForCC();
		}
		@When("^User clicks on guest check out button$")
		public void user_clicks_on_guest_check_out_button() throws Throwable {
		  //ALREADY MENTIONED ON User choose the field for new guest and enters the new email id
		}
		@When("^User selects click & collect option and then continue$")
		public void user_selects_click_collect_option_and_then_continue() throws Throwable {
			checkOut.selectCCAndContinue();
		}
		@When("^User enters post code and find address$")
		public void user_enters_post_code_and_find_address() throws Throwable {
			checkOut.userEnterPostCode();
		}
		@When("^User clicks on proceed to payment button$")
		public void user_clicks_on_proceed_to_payment_button() throws Throwable {
			checkOut.clickOnProceedToPayment();
		}
		@When("^User enters the card details on the payment page$")
		public void user_enters_the_card_details_on_the_payment_page() throws Throwable {
			checkOut.enterCardDetails();
		}
		@When("^User clicks on payment button$")
		public void user_clicks_on_payment_button() throws Throwable {
			checkOut.clicksOnPayment();
		}
		@Then("^User should get payment confirmation$")
		public void user_should_get_payment_confirmation() throws Throwable {
			checkOut.paymentConfirmation();
		}
		//functionality when the guest place an order by home delivery option
				@When("^User selects home delivery option and then continue$")
				public void user_selects_home_delivery_option_and_then_continue() throws Throwable {
					checkOut.selectHDAndContinue();
				}
				@When("^User enters the mandatory fields and find address$")
				public void user_enters_the_mandatory_fields_and_find_address() throws Throwable {
					checkOut.userEnterMandatoryField();
				}
				@When("^User selects the type of home delivery and proceed$")
				public void user_selects_the_type_of_home_delivery_and_proceed() throws Throwable {
					checkOut.selectHD();
				}

		
		
		
		

}
