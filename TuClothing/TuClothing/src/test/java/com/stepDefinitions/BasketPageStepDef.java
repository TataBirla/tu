package com.stepDefinitions;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class BasketPageStepDef extends BaseClass {
	
	//BASKET FUNCTIONALITY
		@Given("^User is on the basket page to add pc$")
		public void user_is_on_the_basket_page_to_add_pc() throws Throwable {
			homepage.verifyHomePage();
			basketPage.userOnBasketPToAddPc();		
		}
		@When("^User clicks on add a promotion code and adds the code$")
		public void user_clicks_on_add_a_promotion_code_and_adds_the_code() throws Throwable {
		    //NEEDS TO ADD PROMOTIONAL CODE
		}
		@When("^User clicks on apply$")
		public void user_clicks_on_apply() throws Throwable {
		    //NEEDS TO DO IT
		}
		@Then("^Promotion code is attached to the payment$")
		public void promotion_code_is_attached_to_the_payment() throws Throwable {
		   //NEEDS TO DO THIS
		}
		@Given("^User is on the basket page$")
		public void user_is_on_the_basket_page() throws Throwable {
			homepage.verifyHomePage();
			basketPage.userOnBasketPToAddPc();
		}
		@When("^User clicks on click & collect option$")
		public void user_clicks_on_click_collect_option() throws Throwable {
			basketPage.userClicksOnCAndC();
		}
		@Then("^User should get the details about click & collect$")
		public void user_should_get_the_details_about_click_collect() throws Throwable {
			basketPage.detailsOfDeliveryInfo();
		}
		@When("^User clicks on free returns to store option$")
		public void user_clicks_on_free_returns_to_store_option() throws Throwable {
			basketPage.userClicksOnFreeReturnsToStore();
		}
		@Then("^User should direct to the returns & refunds page$")
		public void user_should_direct_to_the_returns_refunds_page() throws Throwable {
			basketPage.detailsOfReturnsAndRefunds();
		}
		@When("^User clicks on home delivery option$")
		public void user_clicks_on_home_delivery_option() throws Throwable {
			basketPage.userClicksOnHomeDelivery();
		}
		@Then("^User should get the details about home delivery$")
		public void user_should_get_the_details_about_home_delivery() throws Throwable {
			basketPage.detailsOfDeliveryInfo();
		}
		@When("^User amend the quantity$")
		public void user_amend_the_quantity() throws Throwable {
			basketPage.userAmendsTheQuantity();
		}
		@When("^User clicks on update$")
		public void user_clicks_on_update() throws Throwable {
			basketPage.userClicksOnUpdate();
		}
		@Then("^User should see the updated quantity of the product in the basket page$")
		public void user_should_see_the_updated_quantity_of_the_product_in_the_basket_page() throws Throwable {
			basketPage.updatedAmendedQuantity(); //BETTER TO DO THE ASSERTION OF THE SENTENCE WHICH SHOWS THAT THE PRODUCT QUANTITY IS UPDATED
		}
		@When("^User select remove product option from the list$")
		public void user_select_remove_product_option_from_the_list() throws Throwable {
			basketPage.userRemovingProduct();
		}
		@Then("^Product should remove from the basket page$")
		public void product_should_remove_from_the_basket_page() throws Throwable {
		    //NEEDS TO DO THE ASSERTION FOR THE NEW PAGE
		}
		@When("^User clicks on continue shopping option$")
		public void user_clicks_on_continue_shopping_option() throws Throwable {
			basketPage.userClicksOnContinueShopping();
		}
		@Then("^User should direct to the home page for shopping$")
		public void user_should_direct_to_the_home_page_for_shopping() throws Throwable {
			basketPage.userDirectsForShopping();
		}
		@When("^User selects add to basket button$")
		public void user_selects_add_to_basket_button() throws Throwable {
			 basketPage.userSelectAddToBasket();
		}
		@When("^User clicks on view basket$")
		public void user_clicks_on_view_basket() throws Throwable {
			basketPage.userClicksOnViewBasket();
		}
		@When("^User selects proceed to checkout button$")
		public void user_selects_proceed_to_checkout_button() throws Throwable {
			basketPage.userProceedToCheckOutButton();
		}
		
		
		
		


}
