package com.stepDefinitions;



import com.runner.BaseClass;

import cucumber.api.java.en.Then;

public class SearchResultsPageStepDef extends BaseClass {
	
	@Then("^User should see the related results$")
	public void user_should_see_the_related_results() throws Throwable {	
		  searchresults.searchResultsforValidProduct();
	}
	@Then("^User should see the error message$")
	public void user_should_see_the_error_message() throws Throwable {
		searchresults.searchResultsforInvalidProduct();
	}
	@Then("^User should see the results with product code$")
	public void user_should_see_the_results_with_product_code() throws Throwable {
		searchresults.searchResultsforProductCode();
	}
	@Then("^User should see the related products with color$")
	public void user_should_see_the_related_products_with_color() throws Throwable {
		searchresults.searchResultsforProductColor();
	}
	@Then("^User should see the related products$")
	public void user_should_see_the_related_products() throws Throwable {
		searchresults.searchResultsforProductDescription();
	}
	@Then("^User should see the randomly selected products$")
	public void user_should_see_the_randomly_selected_products() throws Throwable {
		searchresults.searchResultsforSpecialCharacters();
	}
	@Then("^User should see the welcome message with name$") //CANT GO TO FINAL STAGE,BECAUSE OF RECAPTCHA BUT IN REAL IT WILL TAKE TO PRODUCT LIST PAGE
	public void user_should_see_the_welcome_message_with_name() throws Throwable {
		searchresults.welcomeMessageWithName();
	}
}
