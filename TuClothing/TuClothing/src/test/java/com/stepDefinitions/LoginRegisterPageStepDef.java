package com.stepDefinitions;



import java.util.Map;

import com.runner.BaseClass;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginRegisterPageStepDef extends BaseClass {
	
	@When("^User enter valid credentials$")
	public void user_enter_valid_credentials(DataTable userDetails) throws Throwable {
		Map<String, String> loginDetails = userDetails.asMap(String.class, String.class);
		loginRegister.userEnterValidCredentials(loginDetails.get("email"),loginDetails.get("password"));
	}
	
	
//		@When("^User enter valid credentials$")
//		public void user_enter_valid_credentials() throws Throwable {
//			loginRegister.userEnterValidCredentials();
//		}
		@When("^User enter invalid credentials$")
		public void user_enter_invalid_credentials() throws Throwable {
			loginRegister.userEnterInvalidCredentials();
		}
		@Then("^User should see the error message because of invalid credentials$")//CANT GO TO FINAL STAGE,BECAUSE OF RECAPTCHA
		public void user_should_see_the_error_message_because_of_invalid_credentials() throws Throwable {
			loginRegister.errormessageduetoInvalidCredentials();
		}
		@When("^User enter valid user id and an invalid password$")
		public void user_enter_valid_user_id_and_an_invalid_password() throws Throwable {
			loginRegister.userenterValidIdandInvalidPassword();
		}
		@Then("^User should see the error message because of invalid password$")//CANT GO TO FINAL STAGE,BECAUSE OF RECAPTCHA
		public void user_should_see_the_error_message_because_of_invalid_password() throws Throwable {
			loginRegister.errormessageduetoInvalidPassword();
		}
		@When("^User enter invalid user id and valid password$")
		public void user_enter_invalid_user_id_and_valid_password() throws Throwable {
			loginRegister.userenterInvalidIdandValidPassword();
		}
		@Then("^User should see the error message because of invalid user Id$")//CANT GO TO FINAL STAGE,BECAUSE OF RECAPTCHA
		public void user_should_see_the_error_message_because_of_invalid_user_Id() throws Throwable {
			loginRegister.errormessageduetoInvalidId();
		}
		@When("^User click on forgotten your password$")
		public void user_click_on_forgotten_your_password() throws Throwable {
			loginRegister.userClickOnForgottenYourPassword();
		}
		@Then("^User should direct to forgotten your password page$")//CANT GO TO FINAL STAGE,BECAUSE OF RECAPTCHA
		public void user_should_direct_to_forgotten_your_password_page() throws Throwable {
			loginRegister.userDirectToForgottenYourPasswordPage();
		}
		@When("^User kept user id and password as blank$")
		public void user_kept_user_id_and_password_as_blank() throws Throwable {
			loginRegister.userkeptIdAndPasswordAsBlank();
		}
		@When("^User click on recaptcha$")
		public void user_click_on_recaptcha() throws Throwable {
		    //When User click on recaptcha,but cant automate recaptcha
		}
		@Then("^User should see the error message due to blank details$")
		public void user_should_see_the_error_message_due_to_blank_details() throws Throwable {
			loginRegister.userGetErrorMessageDueToBlankdata();
		}
		@When("^User enter valid user id & valid password$")
		public void user_enter_valid_user_id_valid_password() throws Throwable {
			loginRegister.userEnterValidIdAndValidPassword();
		}
		@When("^User kept recaptcha as not selected$")
		public void user_kept_recaptcha_as_not_selected() throws Throwable {
		  //WE CANT DO THIS BECAUSE OF RECAPTCHA
		}
		@Then("^User should see the error message due to without recaptcha$")
		public void user_should_see_the_error_message_due_to_without_recaptcha() throws Throwable {
			loginRegister.errorMessageDueToWIthoutRecaptcha();
		}
		@When("^User click on show password$")
		public void user_click_on_show_password() throws Throwable {
			loginRegister.userClickOnShowPassword();
	    }
		@Then("^User should see the password as bullet points or alphabets$")
		public void user_should_see_the_password_as_bullet_points_or_alphabets() throws Throwable {
			loginRegister.userSeeThePasswordAsBulletPoints();
		}
		//Register Functionality
		@Given("^User is on registration page$")
		public void user_is_on_registration_page() throws Throwable {
			loginRegister.useronRegistrationPage();
		}
		@When("^User click on registration$")
		public void user_click_on_registration() throws Throwable {
			loginRegister.userclicksonCRegisterbutton();
		}
		@Then("^User should get error message$")
		public void user_should_get_error_message() throws Throwable {
			loginRegister.errorMessageForBlankDataonReg();
         }
		@When("^User enters mandatory fields along with invalid email id$")
		public void user_enters_mandatory_fields_along_with_invalid_email_id() throws Throwable {
			loginRegister.userEntersFieldWithInvalidemail();
		}
		@Then("^User should get error message because of invalid email id$")
		public void user_should_get_error_message_because_of_invalid_email_id() throws Throwable {
			loginRegister.warningmessagedueToInvalidEmail();
		}
		@When("^User enters first name and last name with special characters and numbers along with other mandatory fields$")
		public void user_enters_first_name_and_last_name_with_special_characters_and_numbers_along_with_other_mandatory_fields() throws Throwable {
			loginRegister.userEntersWithSpecialCharacters();
		}
		@Then("^User should get error message because of special characters$")
		public void user_should_get_error_message_because_of_special_characters() throws Throwable {
			loginRegister.warningMessageDueToSpecialCharacters();
		}
		@When("^User enters mandatory fields along with password less than (\\d+) characters$")
		public void user_enters_mandatory_fields_along_with_password_less_than_characters(int arg1) throws Throwable {
			loginRegister.userEntersWithPasswordLessThan6Characters();
		}
		@Then("^User should get error message because of less characters$")
		public void user_should_get_error_message_because_of_less_characters() throws Throwable {
			loginRegister.warningMessageForPasswordLessThan6Characters();
		}
		@When("^User enters mandatory fields along with nectar card number$")
		public void user_enters_mandatory_fields_along_with_nectar_card_number() throws Throwable {
			loginRegister.userEntersWithNectarCard();
		}
		@Then("^User should direct to the home page$")//IT WONT WORK BECAUSE OF RECAPTCHA
		public void user_should_direct_to_the_home_page() throws Throwable {
			loginRegister.userDirectToHomePage();
		}
		@When("^User enters all mandatory fields with valid credentials$") //
		public void user_enters_all_mandatory_fields_with_valid_credentials(DataTable userData) throws Throwable {
			loginRegister.userEntersWithValidCredentialsForRegister(userData); 		 
		}

		@Then("^User should direct to the home page as because of successful registration$")
		public void user_should_direct_to_the_home_page_as_because_of_successful_registration() throws Throwable {
			loginRegister.userGetSuccessfulNotification();
		}
		@When("^User enters all mandatory fields without selecting the recaptcha box$")
		public void user_enters_all_mandatory_fields_without_selecting_the_recaptcha_box() throws Throwable {
			loginRegister.userEntersDataWithoutRecaptcha();
		}
		@Then("^User should get an error message due to recaptcha$")
		public void user_should_get_an_error_message_due_to_recaptcha() throws Throwable {
			loginRegister.errorMessageWithoutRecaptcha();
		}
}
