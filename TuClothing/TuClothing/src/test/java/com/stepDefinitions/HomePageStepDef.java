package com.stepDefinitions;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


public class HomePageStepDef extends BaseClass {
	
	
	//Login Functionality
	@Given("^User is on home page$")
	public void user_is_on_home_page() throws Throwable {	
		homepage.verifyHomePage();
	}
	
	@When("^User search for \"([^\"]*)\" keyword$")
	public void user_search_for_keyword(String searchWord) throws Throwable {
	    homepage.searchforValidProduct(searchWord);
	}
	
	@When("^User search for invalid product$")
	public void user_search_for_invalid_product() throws Throwable {
		homepage.searchforinvalidproduct();
	}
	@When("^User leave the search field blank$")
	public void user_leave_the_search_field_blank() throws Throwable {
		homepage.searchwithblankdata();
    }
	@Then("^User should be prompt to fill the search field$")
	public void user_should_be_prompt_to_fill_the_search_field() throws Throwable {
		homepage.promptMessageForBlankData();
	}
	@When("^User search with valid product code$")
	public void user_search_with_valid_product_code() throws Throwable {
		homepage.searchWithProductCode();
	}
	@When("^User search with a product colour$")
	public void user_search_with_a_product_colour() throws Throwable {
		homepage.searchWithProductColor();
	}
	@When("^User search with a product description$")
	public void user_search_with_a_product_description() throws Throwable {
		homepage.searchwithProductDescription();
	}
	@When("^User enter special characters on the search field$")
	public void user_enter_special_characters_on_the_search_field() throws Throwable {
		homepage.searchwithSpecialCharacters();
	}
	//Login Functionality
		@When("^User click on login/register button$")
		public void user_click_on_login_register_button() throws Throwable {
			homepage.userClicksonLoginRegisterButton();
		}
		//END TO END SCENARIOS
		@Given("^User is on the home page$")
		public void user_is_on_the_home_page() throws Throwable {
			homepage.verifyHomePage();
		}

		
}