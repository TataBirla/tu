package com.stepDefinitions;



import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StoreLocatorPageStepDef extends BaseClass {
	
	//STORE LOCATOR FUNCTIONALITY
		@Given("^User is on the store locator page$")
		public void user_is_on_the_store_locator_page() throws Throwable {
			homepage.verifyHomePage();
			storeLocator.userOnStoreLocatorPage();
		}
		@When("^User enters the valid post code$")
		public void user_enters_the_valid_post_code() throws Throwable {
			 storeLocator.userEntersValidPostcode();
		}
		@When("^User clicks on find stores$")
		public void user_clicks_on_find_stores() throws Throwable {
			storeLocator.userClicksOnFindStores();
		}
		@Then("^User should get the details of nearest stores and its opening times$")
		public void user_should_get_the_details_of_nearest_stores_and_its_opening_times() throws Throwable {
			storeLocator.userGetsDetailsOfNearestStores();
		}
		@When("^User enters the invalid post code$")
		public void user_enters_the_invalid_post_code() throws Throwable {
			storeLocator.userEntersInvalidPostCode();
		}
		@Then("^User should get an error message due to invalid postcode$")
		public void user_should_get_an_error_message_due_to_invalid_postcode() throws Throwable {
			storeLocator.errorMessageDueToInvalidPostcode();
		}
		@When("^User enters the town name$")
		public void user_enters_the_town_name() throws Throwable {
			storeLocator.userEntersTownName();
		}
		@Then("^User should get the details of nearest stores and its opening times with town name$")
		public void user_should_get_the_details_of_nearest_stores_and_its_opening_times_with_town_name() throws Throwable {
			storeLocator.userGetsDetailsOfnearestStoreWithOpeningTime();
		}
		@When("^User leaves field as blank$")
		public void user_leaves_field_as_blank() throws Throwable {
			storeLocator.userLeavesFieldAsBlank();
		}
		@Then("^User should get an error message due to blank data$")
		public void user_should_get_an_error_message_due_to_blank_data() throws Throwable {
			storeLocator.userGetsErrorDueToBlankdata();
		}
		@When("^User enters the town name or post code and select the option for stock clothing and click & collect$")
		public void user_enters_the_town_name_or_post_code_and_select_the_option_for_stock_clothing_and_click_collect() throws Throwable {
			storeLocator.userSelectForSCAndCC();
		}
		@Then("^User will be able to see only the stores which is available for stock clothing and click & collect$")
		public void user_will_be_able_to_see_only_the_stores_which_is_available_for_stock_clothing_and_click_collect() throws Throwable {
			storeLocator.storesWithSCAndCC();		
		}
		@When("^User enters the town name or post code$")
		public void user_enters_the_town_name_or_post_code() throws Throwable {
			storeLocator.userEntersWithPCOrTownName();
		}
		@When("^User select the very nearby store$")
		public void user_select_the_very_nearby_store() throws Throwable {
			storeLocator.userSelectVeryNearByStore();
		}
		@Then("^User should be able to see the details of the particular store$")
		public void user_should_be_able_to_see_the_details_of_the_particular_store() throws Throwable {
			storeLocator.particularStoreDetails();
		}

}
