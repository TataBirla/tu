package com.driver;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.runner.BaseClass;

public class Action extends BaseClass {
	
	public void updateElement(By elementName,String keyword) {
		driver.findElement(elementName).clear();
        driver.findElement(elementName).sendKeys(keyword);
	}
	public void clickOnTheElement(By elementName) {
		driver.findElement(elementName).click();
	}
	public void clickOnTheElementWithIndex(By elementName, int index) {
		driver.findElements(elementName).get(index).click();
	}
	
	public void dropDownByIndex(By elementName,int index) {
		Select DropDown = new Select(driver.findElement(elementName));
		DropDown.selectByIndex(index);
	}

}
