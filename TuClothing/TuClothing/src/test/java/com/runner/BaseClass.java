package com.runner;

import org.openqa.selenium.WebDriver;

import com.driver.Action;
import com.driver.Get;
import com.pages.BasketPage;
import com.pages.CheckOutPage;
import com.pages.HomePage;
import com.pages.LoginRegisterPage;
import com.pages.ProductDetailsPage;
import com.pages.ProductListingPage;
import com.pages.SearchResultsPage;
import com.pages.StoreLocatorPage;

public class BaseClass {
	
	public static WebDriver driver;
	public static HomePage homepage = new HomePage();
	public static SearchResultsPage searchresults = new SearchResultsPage();
	public static LoginRegisterPage loginRegister = new LoginRegisterPage();
	public static StoreLocatorPage storeLocator = new StoreLocatorPage();	
	public static Get get = new Get();
	public static Action action = new Action();
	public static ProductDetailsPage productDetails = new ProductDetailsPage();
	public static BasketPage basketPage = new BasketPage();
	public static CheckOutPage checkOut = new CheckOutPage();
	public static ProductListingPage productListing = new ProductListingPage();
}
