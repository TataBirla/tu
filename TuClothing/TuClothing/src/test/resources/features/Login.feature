Feature: Login 
//Due to recaptcha we cant proceed to final step,tried for datatable

Scenario: Verify login with valid credentials
Given User is on home page
When User click on login/register button
And User enter valid credentials
|email|test90@dne.com|
|password|tata123|
Then User should see the welcome message with name

Scenario: Verify login with invalid credentials
Given User is on home page
When User click on login/register button
And User enter invalid credentials
Then User should see the error message because of invalid credentials

Scenario: Verify login with valid user id and an invalid password
Given User is on home page
When User click on login/register button
And User enter valid user id and an invalid password
Then User should see the error message because of invalid password

Scenario: Verify login with invalid user id and valid password
Given User is on home page
When User click on login/register button
And User enter invalid user id and valid password
Then User should see the error message because of invalid user Id

Scenario: Verify login functionality of "Forgotten your password"
Given User is on home page
When User click on login/register button
And User click on forgotten your password
Then User should direct to forgotten your password page

Scenario: Verify login when user id & Password is blank and recaptcha is selected
Given User is on home page
When User click on login/register button
And User kept user id and password as blank
And User click on recaptcha
Then User should see the error message due to blank details

Scenario: Verify login with valid user id & valid password and without Captcha
Given User is on home page
When User click on login/register button
And User enter valid user id & valid password
And User kept recaptcha as not selected
Then User should see the error message due to without recaptcha

Scenario: Verify login functionality of password field is either visible as bullet points or show alphabets
Given User is on home page
When User click on login/register button
And User enter valid credentials
And User click on show password
Then User should see the password as bullet points or alphabets


