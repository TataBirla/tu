Feature: Register 

Scenario: Verify the message when user without filling mandatory field
Given User is on registration page
When User click on registration
Then User should get error message

Scenario: Verify the registration functionality by invalid email id
Given User is on registration page
When User enters mandatory fields along with invalid email id
And User click on registration 
Then User should get error message because of invalid email id

Scenario: Verify if the numbers and special characters are allowed in the first and last name
Given User is on registration page
When User enters first name and last name with special characters and numbers along with other mandatory fields
And User click on registration 
Then User should get error message because of special characters

Scenario: Verify the registration functionality with a password less than 6 characters
Given User is on registration page
When User enters mandatory fields along with password less than 6 characters
And User click on registration 
Then User should get error message because of less characters

Scenario: Verify the registration functionality to add nectar card upon registration
Given User is on registration page
When User enters mandatory fields along with nectar card number
And User click on registration 
Then User should direct to the home page
@Smoke
Scenario: Verify the registration functionality by providing all the valid credentials
Given User is on registration page
When User enters all mandatory fields with valid credentials
|email|Test@test.com|
|firstName|Tata|
|lastName|Birla|
|password|tatabirla1234|
|confirmPassword|tatabirla1234|
|nectarNumber|45678964|
And User click on registration 
Then User should direct to the home page as because of successful registration

Scenario: Verify the registration functionality by providing the already registered credentials //didnt do it for PAGEOBJECTMODEL
Given User is on registration page
When User enters all mandatory fields with already registered credentials
And User click on registration 
Then User should get a prompt message

Scenario: Verify the registration functionality without recaptcha
Given User is on registration page
When User enters all mandatory fields without selecting the recaptcha box
And User click on registration 
Then User should get an error message due to recaptcha


