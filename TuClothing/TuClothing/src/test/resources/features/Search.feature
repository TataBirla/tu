Feature: Search
//we just practised for scenario ouline and parametrized method,but didnt made any change on the code.

Scenario Outline: Verify search with a valid product name
Given User is on home page
When User search for "<keyword>" keyword
Then User should see the related results

Examples:
|keyword|
|tops|
|137430191|
|Black|


Scenario: Verify search with an invalid product name
Given User is on home page
When User search for invalid product
Then User should see the error message

Scenario: Verify search with a blank field
Given User is on home page
When User leave the search field blank
Then User should be prompt to fill the search field

Scenario: Verify search with a valid Product code
Given User is on home page
When User search with valid product code
Then User should see the results with product code

Scenario: Verify search with a product colour
Given User is on home page
When User search with a product colour
Then User should see the related products with color

Scenario: Verify search with a product description
Given User is on home page
When User search with a product description
Then User should see the related products

Scenario: Verify search with all special characters
Given User is on home page
When User enter special characters on the search field
Then User should see the randomly selected products

