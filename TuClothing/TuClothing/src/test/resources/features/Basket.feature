Feature: Basket

Scenario: Verify functionality by adding a promotion code
Given User is on the basket page to add pc
When User clicks on add a promotion code and adds the code
And User clicks on apply
Then Promotion code is attached to the payment

Scenario: Verify the functionality for click & collect
Given User is on the basket page
When User clicks on click & collect option
Then User should get the details about click & collect

Scenario: Verify the functionality for free returns to store
Given User is on the basket page
When User clicks on free returns to store option
Then User should direct to the returns & refunds page

Scenario: Verify the functionality for home delivery
Given User is on the basket page
When User clicks on home delivery option
Then User should get the details about home delivery

Scenario: Verify the functionality of adding multiple quantity of the same product to the basket
Given User is on the basket page
When User amend the quantity
And User clicks on update
Then User should see the updated quantity of the product in the basket page

Scenario: Verify the functionality of removing a product from the basket page
Given User is on the basket page
When User select remove product option from the list
Then Product should remove from the basket page

Scenario: Verify the functionality of continue shopping from the basket page
Given User is on the basket page
When User clicks on continue shopping option
Then User should direct to the home page for shopping
