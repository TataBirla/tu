Feature: Store Locator

Scenario: Verify the functionality with valid postcode
Given User is on the store locator page
When User enters the valid post code
And User clicks on find stores
Then User should get the details of nearest stores and its opening times

Scenario: Verify the functionality with invalid postcode
Given User is on the store locator page
When User enters the invalid post code
And User clicks on find stores
Then User should get an error message due to invalid postcode

Scenario: Verify the functionality with town name
Given User is on the store locator page
When User enters the town name
And User clicks on find stores
Then User should get the details of nearest stores and its opening times with town name

Scenario: Verify the functionality by without entering any keywords on the field
Given User is on the store locator page
When User leaves field as blank
And User clicks on find stores
Then User should get an error message due to blank data

Scenario: Verify the functionality of the filters for stock clothing and click & collect
Given User is on the store locator page
When User enters the town name or post code and select the option for stock clothing and click & collect
And User clicks on find stores
Then User will be able to see only the stores which is available for stock clothing and click & collect

Scenario: Verify the functionality of the particular store by clicking from the list
Given User is on the store locator page
When User enters the town name or post code
And User clicks on find stores
And User select the very nearby store
Then User should be able to see the details of the particular store


