Feature: Checkout
//CANT DO REGISTERED USERS CASES AS BECAUSE OF RECAPTCHA,BELOW REGISTERED CREDENTIALS MEAN GUEST EMAIL ONLY

Scenario: Verify the functionality of click & collect if the user is purchasing below �20.00
Given User is on the checkout page
When User enters registered credentials to proceed
And User clicks on continue
Then User should get disabled option for click & collect

Scenario: Verify the functionality with home delivery option for new guest 
Given User is on the checkout page
When User choose the field for new guest and enters the new email id
And User clicks on continue for delivery
And User selects home delivery option
Then User should get an enabled option to continue to check out

Scenario: Verify the functionality with click & collect option for new guest
Given User is on the checkout page
When User choose the field for new guest and enters the new email id
And User clicks on continue for click & collect
And User selects click & collect option
Then User should get an enabled option to continue to check out to proceed
