Feature: Add Product To Basket

Scenario: Verify adding a product to basket without login details
Given User is on product details page
When User select size and quantity
And User click on add to basket
Then Product should add to the basket

Scenario: Verify adding a product to basket with login details
Given User is already logged in and is on product details page
When User select size and quantity
And User click on add to basket
Then Product should add to the basket with login details

Scenario: Verify the functionality by adding multiple quantity to the basket
Given User is on product details page
When User select size and multiple quantities
And User click on add to basket
Then Product should add to the basket with multiple quantity

Scenario: Verify the functionality by removing the product from the basket
Given User is on product details page
When User select remove product option
Then Product should remove from the basket
